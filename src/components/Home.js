import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import LoginForm from './LoginForm';
import '../styles/home.css';

class Home extends React.Component {
  render() {
    return (
      <MuiThemeProvider>
        <LoginForm {...this.props}/>
      </MuiThemeProvider>
    );
  }
}

export default Home;
