import React from 'react';
import Paper from 'material-ui/Paper';

import './../styles/projectsList.css';

class ProjectsList extends React.Component {
  render() {
    return (
      <div className="container-paper">
        <div>
          <Paper style={{width: "300px", height: "400px", marginLeft: "20px" ,padding: "10px"}} zDepth={2} rounded={false}>
            Projeto A
          </Paper>
        </div>
        <div className="container-list">
          <Paper style={{width: "700px", height: "400px", marginLeft: "20px" ,padding: "10px"}} zDepth={2} rounded={false}>
            Lista de Projetos
          </Paper>
        </div>
      </div>
    );
  }
}

export default ProjectsList;
