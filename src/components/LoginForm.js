import React from 'react';
import Paper from 'material-ui/Paper';
import './../styles/login.css';

class LoginForm extends React.Component {

    redirectToProjects = () => {
    let {transition} = this.props;
    transition.router.stateService.go('projects', {}, {});
  }

    render() {
        return (
              <Paper style={{width: "30%", margin: "100px auto", padding: "10"}}zDepth={2} rounded={false}>
                <form>
                    <div className="form-group">
                      <label className="input-label">Email<input className="input-form" type="text"/></label>
                      <label className="input-label">Senha<input className="input-form" type="password"/></label>
                    </div>
                    <div className="div-buttons">
                      <button className="login-button" onClick={this.redirectToProjects}>Entrar</button>
                      <button className="sign-up-button" onClick={this.redirectToProjects}>Novo Usuário</button>
                    </div>
              </form>
              </Paper>
        );
    }
}

export default LoginForm;
