import React from 'react';

import ProjectsList from './ProjectsList';

import './../styles/projects.css';

class Projects extends React.Component {
  render() {
    return (
      <div className="projects-container">
        <div className="title-container">
          <h1>Projetos</h1>
        </div>
        <ProjectsList/>
      </div>
    );
  }
}

export default Projects;
