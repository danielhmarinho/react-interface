import React from 'react';
import FontIcon from 'material-ui/FontIcon';
import './../styles/navbar.css';


const Navbar = () => (
  <div className="navbar-bar">
    <a href="/" className="navbar-item navbar-icon"><FontIcon style={{color: "white"}}className="material-icons">home</FontIcon></a>
    <a href="#" className="navbar-item navbar-icon"><FontIcon style={{color: "white"}} className="material-icons">face</FontIcon></a>
    <a href="/projects" className="navbar-item navbar-icon"><FontIcon style={{color: "white"}} className="material-icons">computer</FontIcon></a>
    <a href="#" className="navbar-item navbar-icon search-icon"><FontIcon style={{color: "white"}} className="material-icons">search</FontIcon></a>
  </div>
);

export default Navbar;
