import React from 'react';
import ReactDOM from 'react-dom';
import {UIRouter, UIView, pushStateLocationPlugin} from 'ui-router-react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Home from './components/Home';
import Projects from './components/Projects';
import Navbar from './components/Navbar';
import Footer from './components/Footer';

import './styles/home.css';
import './index.css';
import './styles/navbar.css';

// define your states
const states = [{
  name: 'app',
  url: '/',
  component: Home
}, {
  name: 'projects',
  url: '/projects',
  component: Projects
}];

// select your plugins
const plugins = [
  pushStateLocationPlugin
];

ReactDOM.render(
  <MuiThemeProvider>
  <div className="page-style">
    <Navbar/>
    <UIRouter plugins={plugins} states={states}>
      <UIView/>
    </UIRouter>
    <Footer/>
  </div>
</MuiThemeProvider>,
  document.getElementById('root')
);
